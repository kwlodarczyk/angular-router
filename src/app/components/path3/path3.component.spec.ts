import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Path3Component } from './path3.component';

describe('Path3Component', () => {
  let component: Path3Component;
  let fixture: ComponentFixture<Path3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Path3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Path3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
