import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { LayoutComponent } from './components/layout/layout.component';
import { Path1Component } from './components/path1/path1.component';
import { Path2Component } from './components/path2/path2.component';
import { Path3Component } from './components/path3/path3.component';
import { CoreComponent } from './components/core/core.component';
import { HomeComponent } from './components/home/home.component';

@NgModule({
  declarations: [
    LayoutComponent,
    Path1Component,
    Path2Component,
    Path3Component,
    CoreComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [CoreComponent]
})
export class AppModule { }
