import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LayoutComponent} from './components/layout/layout.component';
import {Path1Component} from './components/path1/path1.component';
import {Path2Component} from './components/path2/path2.component';
import {Path3Component} from './components/path3/path3.component';
import {HomeComponent} from './components/home/home.component';


const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {
    path: 'home', component: LayoutComponent,
    children: [
      {path: '', component: HomeComponent},
      {path: 'path-1', component: Path1Component},
      {path: 'path-2', component: Path2Component},
      {path: 'path-3', component: Path3Component}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
